(function($) {
    $(document).ready(function($) {
        if (typeof WOW === 'function') {
            // Check if class exists, then initialize WOW object.
            new WOW().init();
        }
    });
})(jQuery);


(function($) {
    $(document).ready(function() {
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll > 100) {
                $(".page-header").css("height", "70px");
            } else {
                $(".page-header").css("height", "90px");
            }
        })
    })
}(jQuery));


(() => {
    var elem = document.getElementById("navbarSupportedContent");
    document.querySelector(".navbar-toggler").addEventListener("click", () => {


        window.setInterval(() => {
            if (elem.classList.contains("show")) {
                document.querySelector(".page-header").style.height = "100px";
            } else {
                $(".page-header").css("height", "90px");
            }

        }, 100);


    })

})();